'use strict';
/**
 * The core of the library. It provides timing and execution, leaving control
 * over what's drawn and how the logic works to the user.
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 * @module engine/base
 * @version 0.1.0
 */

var height, width;

var bufferContext, context;
var buffer;

function createCanvas(_width, _height) {
	buffer = document.createElement('canvas');
	var canvas = document.createElement('canvas');
	document.body.appendChild(canvas);

	height = buffer.height = canvas.height = _height;
	width = buffer.width = canvas.width = _width;

	bufferContext = buffer.getContext('2d');
	context = canvas.getContext('2d');
}

function rgba(red, green, blue, alpha) {
	alpha = typeof alpha === 'undefined' ? 1.0 : alpha / 100;
	return 'rgba(' + red % 256 + ',' + green % 256 + ',' + blue % 256 + ',' + alpha + ')';
}

function ellipse(x, y, minor, major, rotation, start, end) {
	rotation = typeof rotation === 'undefined' ? 0 : rotation;
	start= typeof start === 'undefined' ? 0 : start;
	end = typeof end === 'undefined' ? 2 * Math.PI : end;
	bufferContext.beginPath();
	bufferContext.ellipse(x, y, minor, major, rotation, start, end);
}

function line(x0, y0, x1, y1) {
	bufferContext.beginPath();
	bufferContext.moveTo(x0, y0);
	bufferContext.lineTo(x1, y1);
}

function rectangle(x, y, width, height) {
	bufferContext.beginPath();
	bufferContext.rect(x, y, width, height);
}

function fill(red, green, blue, alpha) {
	bufferContext.fillStyle = rgba(red, green, blue, alpha);
	bufferContext.fill();
}

function stroke(red, green, blue, alpha) {
	bufferContext.strokeStyle = rgba(red, green, blue, alpha);
	bufferContext.stroke();
}

function random(min, max) {
	return Math.floor(Math.random() * Math.floor(max) + min);
}

function translate(x, y) {
	bufferContext.translate(x, y);
}

window.onload = function() {
	if (typeof prepare !== 'function' || typeof draw !== 'function') {
		console.log('Missing prepare() or draw()');
		return;
	}

	// Initialisation
	prepare();

	// Rendering
	setInterval(function() {
		draw();

		// Copy the buffer onto the canvas
		context.drawImage(buffer, 0, 0);

		// Clear the buffer
		bufferContext.fillStyle = rgba(0, 0, 0);
		bufferContext.fillRect(0, 0, buffer.width, buffer.height);
	}, 1000 / 60);
};
