/**
 *
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 * @module engine/util
 * @version 1.0.0
 */
function random(array) {
	return array[Math.floor(Math.random() * array.length)];
}

function randomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);

	return Math.floor(Math.random() * (max - min)) + min;
}
