function Star() {
	var x = random(0, width);
	var y = random(0, height);
	var z = width;

	this.draw = function() {
		fill(255, 255, 255);

		var sx = (x / z) * width;
		var sy = (y / z) * height;

		ellipse(sx, sy, 4, 4);
		z--;
	}
}

var stars = [];

function prepare() {
	createCanvas(400, 400);
	//translate(width / 2, height / 2);
	for (var i = 0; i < 100; i++) {
		stars[i] = new Star();
	}
}

function draw() {
//	translate(width / 2, height / 2);
	for (var i = 0; i < 100; i++) {
		stars[i].draw();
	}
}
