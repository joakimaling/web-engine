# web-engine

> Small modular web-based game engine

[![Licence][licence-badge]][licence-url]

${introduction}.

## Installation

${installation-instructions}:

```sh

```

## Usage

${usage-instructions}:

```js
var engine = new Engine();

engine.draw(function(canvas) {
	// redraw the canvas
});

engine.tick(function() {
	// do logical updates
});

engine.run();
```

## Licence

Released under MIT. See [LICENSE][licence-url] for more.

Coded with :heart: by [carolinealing][user-url].

[licence-badge]: https://img.shields.io/gitlab/license/carolinealing/web-engine.svg
[licence-url]: LICENSE
[user-url]: https://gitlab.com/carolinealing
