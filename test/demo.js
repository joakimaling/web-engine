/**
 * A demo to test the various features of the engine.
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 */
window.onload = function() {
	var colour = '#00ff46';
	var size = 24;

	var glyphs = [
		0x30A2, 0x30A4, 0x30A6, 0x30A8, 0x30AA, 0x30AB, 0x30AD, 0x30AF,
		0x30B1, 0x30B3, 0x30B5, 0x30B7, 0x30B9, 0x30BB, 0x30BD, 0x30BF,
		0x30C1, 0x30C4, 0x30C6, 0x30C8, 0x30CA, 0x30CB, 0x30CC, 0x30CD,
		0x30CE, 0x30CF, 0x30D2, 0x30D5, 0x30D8, 0x30DB, 0x30DE, 0x30DF,
		0x30E0, 0x30E1, 0x30E2, 0x30E4, 0x30E6, 0x30E8, 0x30E9, 0x30EA,
		0x30EB, 0x30EC, 0x30ED, 0x30EF, 0x30F0, 0x30F1, 0x30F2, 0x30F3,

		0xFF0A, 0xFF0B, 0xFF10, 0xFF11, 0xFF12, 0xFF13, 0xFF14, 0xFF15,
		0xFF17, 0xFF18, 0xFF19, 0xFF1A, 0xFF1D, 0xFF3A
	];

	function Symbol(_x, _y) {
		var delay;
		var glyph;
		var x = _x;
		var y = _y;

		var ticker = 0;

		this.changeDelay = function() {
			delay = randomInt(5, 20);
		}

		this.changeGlyph = function() {
			glyph = String.fromCharCode(
				random(glyphs)
			);
		}

		this.draw = function(context) {
			context.fillStyle = colour;
			context.font = size + 'px serif';
			context.textBaseline = 'middle';
			context.textAlign = 'center';

			if (
				[0xFF12, 0xFF15, 0xFF19].includes(glyph.charCodeAt(0)) ||
				glyph.charCodeAt(0) <= 0x30F3
			) {
				context.scale(-1, 1);
				context.fillText(glyph, -x, y);
				context.scale(-1, 1);
			}
			else if (glyph.charCodeAt(0) == 0xFF13) {
				context.scale(1, -1);
				context.fillText(glyph, x, -y);
				context.scale(1, -1);
			}
			else {
				context.fillText(glyph, x, y);
			}
		}

		this.tick = function() {
			if (ticker++ == delay) {
				this.changeGlyph();
				ticker = 0;
			}
		}
	}

	function Stream() {
		var delay = randomInt(3, 15);
		var symbols = [];
		var total = randomInt(5, 30);
		var y = size / 2;

		var ticker = 0;

		this.draw = function(context) {
			symbols.forEach(function(symbol) {
				symbol.draw(context);
			});
		}

		this.tick = function() {
			if (ticker++ == delay) {
				symbol = new Symbol(engine.width / 2, y);
				symbol.changeDelay();
				symbol.changeGlyph();

				symbols.push(symbol);
				y += size;
				ticker = 0;
			}

			symbols.forEach(function(symbol) {
				symbol.tick();
			});
		}
	}

	var engine = new Engine({
		height: window.innerHeight,
		width: window.innerWidth
	});

	var stream = new Stream();

	engine.draw(function(context) {
		stream.draw(context);
	});

	engine.tick(function() {
		stream.tick();
	});

	engine.start();
};
