/**
 *
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 * @module engine/util
 * @version 1.0.0
 */
function arc(x, y, radius, ) {
	ellipse();
}

function circle(x, y, radius) {
	ellipse(x, y, radius, radius, 0, );
}

function ellipse(x, y, radiusX, radiusY, colour, fill) {
	context.setLineDash([]);
	context.beginPath();
	context.ellipse(x, y, radiusX, radiusY, 0, 0, 2 * Math.PI);
	context.stroke();
}

function rectange(x, y, width, height, colour, fill) {
	if (fill) {
		context.fillStyle = colour;
		context.fillRect(x, y, width, height);
	}
	else {
		context.strokeStyle = colour;
		context.rect(x, y, width, height);
	}
}

function random(array) {
	return array[Math.floor(Math.random() * array.length)];
}

function randomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);

	return Math.floor(Math.random() * (max - min)) + min;
}
