function Maths() {
	/**
	 * Returns a pseudo-random value beteween minimum and maximum, or, if a
	 * single value was given, between 0 and that value.
	 *
	 * @param {number} minimum dsf
	 * @param {number} maximum sdf
	 * @return {number} sdfsd
	 */
	this.random = function(minimum, maximum) {
		if (typeof maximum === 'undefined') {
			maximum = minimum;
			minimum = 0;
		}

		return Math.floor(Math.random() * Math.floor(maximum) + minimum);
	}
}
