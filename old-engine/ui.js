'use strict';
/**
 *
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 * @module engine/ui
 * @version 1.0.0
 */
function UI(canvas) {
	var buttons = [];

	this.createButton = function(options) {
		buttons.push({
//			callback: options.callback,
//			font: options.font,
//			key: options.key,
			text: options.text,
			x: options.x,
			y: options.y
		});
	}

	this.draw = function(context) {
		buttons.forEach(function(button) {
			context.strokeStyle = 'red';
			context.font = '12px Arial';

			context.textAlign = 'left';
			context.textBaseline = 'top';

			console.log(
				button.x,
				button.y,
				context.measureText(button.text).width + 50,
				parseInt(context.font) + 20
			);

			context.rect(
				button.x,
				button.y,
				context.measureText(button.text).width + 50,
				parseInt(context.font) + 20
			);

			context.fillText(
				button.text,
				button.x + 25,
				button.y + 10
			);

			context.stroke();
		});
	}
}
