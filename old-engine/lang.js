'use strict';
/**
 *
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 * @module engine/lang
 * @version 1.0.0
 */
function Lang() {
	var fallback = 'en';
	var list = {};

	/**
	 * Validates the given language code.
	 *
	 * @param {string} code
	 * @return {bool}
	 */
	var validate = function(code) {
		if (!(typeof code === 'string' && code.length == 2)) {
			console.error('[lang] invalid language code');
			return false;
		}

		return true;
	}

	/**
	 * Changes the fallback language to whatever is provided unless it's
	 * invalid.
	 *
	 * @param {string} code
	 */
	this.fallback = function(code) {
		if (validate(code)) {
			fallback = code;
		}
	}

	/**
	 * Does the same as @see get but also replaces parts of the string with
	 * values from the given array/object. If the language isn't specified
	 * the fallback will be used.
	 *
	 * @param {string} key
	 * @param {array|object} values
	 * @param {string} code
	 * @return {string}
	 */
	this.format = function(key, values, code) {
		var str = this.get(key, code);

		for (var i in values) {
			str = str.replace(new RegExp('\\{' + i + '\\}', 'gi'), values[i]);
		}

		return str;
	}

	/**
	 * @param {string} filename
	 * @param {string} code
	 */
	this.getFromFile = function(filename, code) {

	}

	/**
	 * Retrieves a language string with the given key. If the language isn't
	 * isn't specified the fallback will be used.
	 *
	 * @param {string} key
	 * @param {string} code
	 * @return {string}
	 */
	this.get = function(key, code) {
		code = code || fallback;

		// checks if given language exists
		if (list[code] == null) {
			console.error('[lang] language "%s" not found', code);

			// checks for the key in the fallback language
			if (code != fallback) {
				this.get(key, fallback);
			}

			return;
		}

		// checks if given key exists in given language
		if (list[code][key] == null) {
			console.error('[lang] key "%s" in "%s" not found', key, code);
			return;
		}

		return list[code][key];
	}

	/**
	 * Adds a new, or modifies an existing, language string. If the language
	 * isn't specified the fallback will be used.
	 *
	 * @param {string} key
	 * @param {string} value
	 * @param {string} code
	 */
	this.put = function(key, value, code) {
		code = code || fallback;

		if (list[code] == null) {
			list[code] = {};
		}

		list[code][key] = value;
	}
}
