/**
 * A simple input class for the engine.
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 * @module engine/input
 * @version 1.0.0
 */
function Input(canvas) {
	this.keyDown = function(callback) {
		canvas.addEventListener('keydown', function(event) {
			callback(event);
		}, false);
	}

	this.mouseDown = function(callback) {
		canvas.addEventListener('mousedown', function(event) {
			callback(event);
		}, false);
	}
}
