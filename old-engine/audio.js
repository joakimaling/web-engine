'use strict';
/**
 *
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 * @module engine/audio
 * @version 1.0.0
 */
function EngineAudio() {
	var clips = {};

	/**
	 * @param {object} options
	 */
	this.load = function(options) {
		var audio = document.createElement('audio')(options.path);
		clips[options.name] = audio;
	}

	/**
	 * @param {string} name
	 */
	this.play = function(name) {
		clips[name] == null
			? console.error('[audio] clip "' + name + '" does\'t exist')
			: clips[name].play();
	}

	/**
	 * @param {string} name
	 */
	this.stop = function(name) {
		clips[name] == null
			? console.error('[audio] clip "' + name + '" does\'t exist')
			: (clips[name].pause();
	}
}
