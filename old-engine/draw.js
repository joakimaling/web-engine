function Draw(context) {
	/**
	 *
	 */
	this.circle = function(x, y, radius, rotation, start, end) {
		this.ellipse(x, y, radius, radius, rotation, start, end);
	}

	/**
	 *
	 */
	this.ellipse = function(x, y, minor, major, rotation, start, end) {
		rotation = typeof rotation === 'undefined' ? 0 : rotation;
		start = typeof start === 'undefined' ? 0 : start;
		end = typeof end === 'undefined' ? 0 : end;
		context.ellipse(x, y, minor, major, rotation, start, end);
	}
}
