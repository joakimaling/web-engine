'use strict';
/**
 * The core of the game engine. This class provides timing and execution,
 * Leaving control over what's drawn and how the logic works to the
 * designer of the game.
 * It takes an optional object which contains data about surface for the game
 *
 * {
 *   canvas - the canvas object or the id of one
 *   height - the height of the canvas (overrides the canvas' height)
 *   width  - the width of the canvas (overrides the canvas' width)
 * }
 *
 * @author Caroline Åling <caroline@penguinlair.com>
 * @license MIT
 * @module engine/base
 * @version 1.0.0
 */
function Engine(options) {
	// get the canvas
	var canvas = typeof options.canvas === 'string'
		? document.getElementById(options.canvas)
		: options.canvas;

	// create a canvas if none was provided
	if (!canvas) {
		canvas = document.createElement('canvas');
		document.body.appendChild(canvas);
	}

	// override its height if necessary
	if (options.height) {
		canvas.height = options.height;
	}

	// override its width if necessary
	if (options.width) {
		canvas.width = options.width;
	}

	// for dubble buffering
	var buffer = document.createElement('canvas');

	// store the dimensions
	this.height = buffer.height = canvas.height;
	this.width = buffer.width = canvas.width;

	var bufferContext = buffer.getContext('2d');
	// get the context on which to draw
	var context = canvas.getContext('2d');

	// some variables
	var drawCallback;
	var tickCallback;
	var loop = null;

	//
	if (typeof Draw === 'function') {
		this.draw = new Draw(bufferContext);
	}

	// add input functions if the input class was imported
	if (typeof Input === 'function') {
		this.input = new Input(canvas);
		canvas.tabIndex = 0;
	}

	//
	if (typeof Maths === 'function') {
		this.maths = new Maths();
	}

	//
	if (typeof UI === 'function') {
		this.ui = new UI(canvas);
	}

	/**
	 * Runs the main game loop.
	 */
	var run = function() {
		loop = setInterval(function() {
			drawCallback(bufferContext);

			// copy the buffer onto the canvas
			context.drawImage(buffer, 0, 0);

			// clear the buffer
			bufferContext.fillStyle = '#000';
			bufferContext.fillRect(0, 0, buffer.width, buffer.height);

			tickCallback();
		}, 1000 / 60);
	}

	/**
	 * Stores the callback for the draw routine.
	 *
	 * @callback
	 */
	this.draw = function(callback) {
		drawCallback = callback;
	};

	/**
	 * Starts the main game loop.
	 */
	this.start = function() {
		if (!drawCallback || !tickCallback) {
			return;
		}

		this.stop();
		run();
	}

	/**
	 * Stops the main game loop.
	 */
	this.stop = function() {
		if (loop) {
			clearInterval(loop);
			loop = null;
		}
	}

	/**
	 * Stores the callback for the tick routine.
	 *
	 * @callback
	 */
	this.tick = function(callback) {
		tickCallback = callback;
	};
}
